import time


class Trigger:
    def __init__(self,interval):
        self.oldTriggerTime=0
        self.interval = interval
    def aktion(self):
        now=time.time()
        if now-self.oldTriggerTime>=self.interval:
            self.oldTriggerTime=now
            return True
        return False
