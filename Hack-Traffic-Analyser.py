
import threading
from secrets1 import ip, InWledUrl,OutWledUrl,MqttServer,user,pw
from getTraffic_class import *
from led_stripe_class import *
from trigger_class import *
from ProjektStructures import retTraffic
from ProjektStructures import Effect
import time
import requests
import paho.mqtt.client as mqtt
import os


 
# function section
inBytes = getTraffic(ip, 161, '1.3.6.1.2.1.31.1.1.1.6.9',"public",10)
outBytes = getTraffic(ip, 161, '1.3.6.1.2.1.31.1.1.1.10.9', "public",10)
Trigger_20s =Trigger(20.0)
Trigger_10s = Trigger(1)
inTraffic=retTraffic()
outTraffic = retTraffic()
MqttMsgTopic=''
MqttMsgData=''
MqttMsgExist=False

fx=Effect.Theater.value
sx=180
ix=255

def setON_OFF(MqttMsgData,MqttMsgTopic):
    print('1',MqttMsgTopic,MqttMsgData)
    if MqttMsgTopic !='pwr-trafficVisu/power':
     print('-99')
     return

    if MqttMsgData==b'ON':
        print('ON')
        return True
    print('OFF')
    return False

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.connected_flag=True #set flag


def on_message(client, Data1, msg):
    #print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    global MqttMsgData 
    global MqttMsgExist
    global MqttMsgTopic
    MqttMsgData=msg.payload
    MqttMsgTopic=msg.topic
    print(MqttMsgData)
    MqttMsgExist=True
    client.publish('stat/pwr-trafficVisu/POWER',msg.payload)


def on_publish(client, mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(client, mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(client, mqttc, obj, level, string):
    print(string)

class MyThread (threading.Thread):

    def __init__(self, DATEN: retTraffic, url: str, maxLedCount, LED_Start,  revers,id):
        self.strip = LED_Stripe(maxLedCount,"ffffff","000000" , revers)
        self.Led_start = LED_Start
        self.Led_Anzahl= maxLedCount
        self.data = DATEN
        self.id = id
        self.lastBytes = DATEN.Mbyte
        self.NewDATA = True
        self.url = url + "json/state"
        self.lock = threading.RLock()
        super(MyThread, self).__init__()

    def set_Data(self, DATEN: retTraffic):  # you can use a proper setter if you want
        with self.lock:
            #print(DATEN.speed)
            if DATEN.speed > 0.0 and DATEN.speed< 100.1:
                #print('now')
                self.data = DATEN
            self.data.power=DATEN.power
            self.NewDATA = True


    def run(self):
        while True:
            with self.lock:
                #self.strip.MoveByTick()
                
             
                if self.NewDATA:
                    
                    
                    self.NewDATA =False
                    
                    self.strip.Color
                    
                    #self.strip.injetPaket()
                    gliter =int(self.data.speed * 2.55)
                    r=0
                    g=0
                    rgb_fr=[0,0,0]
                    rgb_ba=[0,0,0]
                    faktor=0.8-(0.1*self.data.speed/100)
                    bri=self.data.gleitenderSpeed /100
                    if bri<0.5:
                        bri=0.5
                    if self.data.gleitenderSpeed<50:
                        g=255
                        r=int(2.55*self.data.gleitenderSpeed)
                    else:
                        r=255
                        g=int(255-(2.55*self.data.gleitenderSpeed))
                    if fx==Effect.Theater.value:
                        rgb_ba[0]=str(int(r*bri))
                        rgb_ba[1]=str(int(g*bri))
                        rgb_fr[0]=str(int(r*faktor*bri))
                        rgb_fr[1]=str(int(g*faktor*bri))
                        gliter =int(255-self.data.speed * 2.55)
                        if gliter<20:
                            gliter=20
                    
                    print(gliter)
                    pwr='false'
                    if self.data.power:
                        pwr='true'

                    #print(r,g,(2.55*self.data.gleitenderSpeed*2))
                    col='[['+rgb_fr[0]+','+rgb_fr[1]+',0],['+rgb_ba[0]+','+rgb_ba[1]+',0],[0,0,0]]'
                    jsonstr='{"on":'+pwr+',"bri":'+str(100)+',"transition":7,"ps":-1,"pl":-1,"nl":{"on":false,"dur":60,"mode":1,"tbri":0,"rem":-1},"udpn":{"send":false,"recv":true},"lor":0,"mainseg":0,"seg":[{"id":0,"start":0,"stop":50,"len":50,"grp":1,"spc":0,"of":0,"on":true,"frz":false,"bri":255,"cct":127,"col":'+col+',"fx":'+str(fx)+',"sx":'+str(sx)+',"ix":'+str(gliter)+',"pal":0,"c1":128,"c2":128,"c3":16,"sel":true,"rev":false,"mi":false,"o1":false,"o2":false,"o3":false,"si":0,"m12":0}]}'
                    #print(str(gliter))
                    #print(r,g,self.data.speed,self.data.gleitenderSpeed)
                    try:
                        
                        ret=requests.post(self.url, data=jsonstr, headers={"Content-Type": "application/json"})
                    except:
                        pass
                    #print (ret.content)
                 
                    
           
         
            time.sleep(0.2)  # let it breathe




ReadInData = MyThread(inTraffic,InWledUrl,30,0,False,1)
ReadInData.start()
ReadOutData = MyThread(outTraffic,OutWledUrl, 30,30, False,2)
ReadOutData.start()



hostname = MqttServer #example
      
response = 1

# Create NeoPixel object with appropriate configuration.

# Intialize the library (must be called once before other functions).

mqtt.Client.connected_flag=False#create flag in class
client = mqtt.Client()
client.username_pw_set(user,pw)
client.on_connect = on_connect
client.on_message = on_message

client.connect(MqttServer, 1883, 60)


client.subscribe("pwr-trafficVisu/#", 0)
client.loop_start()

MqttMsgExist = False

while (True):

    
    
    
    if Trigger_10s.aktion():
        start = (time.time())  
         
        inTraffic=inBytes.getMikrotikTraffic()
        inTraffic.power=setON_OFF(MqttMsgData,MqttMsgTopic)
        ReadInData.set_Data(inTraffic)
        outTraffic = outBytes.getMikrotikTraffic()
        outTraffic.power=setON_OFF(MqttMsgData,MqttMsgTopic)
        ReadOutData.set_Data(outTraffic)

    
    
    
    time.sleep(0.01)


    
          
 
