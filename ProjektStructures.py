from dataclasses import dataclass
from enum import Enum
class Effect(Enum):
    Theater = 13


@dataclass
class retTraffic:
    bits: float = 0.0
    Mbyte: float = 0.0
    diff_bits: float = 0.0
    diff_Mbyte: float = 0.0
    diff_Time: float = 0.0
    speed: float = 0.0
    gleitenderSpeed: float =0.0
    done: bool = True
    power: bool = False



    

