from pysnmp.hlapi import *
from ipaddress import *
from ProjektStructures import *
import time




class getTraffic:
    def __init__(Mikrotik, ip, port, OID, community_string,avarage):
       
        Mikrotik.community_string = community_string  # From file
        Mikrotik.ip = ip  # From file
        Mikrotik.port = port
        Mikrotik.OID = OID
        Mikrotik.oldBits = 0
        Mikrotik.LastTime = 0
        Mikrotik.LastSpeedData=[0]
        Mikrotik.avarage=avarage
    def snmp_getcmd(Mikrotik):
        return (getCmd(SnmpEngine(),
                       CommunityData(Mikrotik.community_string),
                       UdpTransportTarget((Mikrotik.ip, Mikrotik.port)),
                   ContextData(),
                       ObjectType(ObjectIdentity(Mikrotik.OID))))


    def snmp_get_next(Mikrotik):
        
        errorIndication, errorStatus, errorIndex, varBinds = next(
            Mikrotik.snmp_getcmd())
        for name, val in varBinds:

            return (val.prettyPrint())


    def getMikrotikTraffic(Mikrotik) -> retTraffic:
        ret=retTraffic()
        Mbyte = 1024/8
        Mbyte *= 1024
        
        #get Data from Mikrotik as complete bits traffic
        try:
            
            ret.bits = int(Mikrotik.snmp_get_next())

            ret.Mbyte = ret.bits/Mbyte
            now=time.time()
            ret.diff_Time = now - Mikrotik.LastTime
            
            ret.diff_bits = ret.bits - Mikrotik.oldBits
            ret.diff_Mbyte = ret.diff_bits/Mbyte
            
            #diff merker
            Mikrotik.oldBits = ret.bits
            Mikrotik.LastTime=now
        except Exception as e: 
            print(e)
            
            ret.bits = 0
            ret.Mbyte = 0
            
            ret.diff_Time = 0
            
            ret.diff_bits = 0
            ret.diff_Mbyte = 0

        
        
        #ret.speed = float(ret.diff_Mbyte/ret.diff_Time)
        ret.speed = float(ret.diff_Mbyte)
        if ret.speed>0.1 and ret.speed<130.0:
            Mikrotik.LastSpeedData.append(ret.speed)
            if len(Mikrotik.LastSpeedData)>Mikrotik.avarage:
                del Mikrotik.LastSpeedData[0]
        ges=0.0
        for val in Mikrotik.LastSpeedData:
            ges+=val
       
        ret.gleitenderSpeed=ges/len(Mikrotik.LastSpeedData)
        #print(len(Mikrotik.LastSpeedData),ges,ret.gleitenderSpeed,ret.speed)
        #print(Mikrotik.LastSpeedData)
        return ret
